const BASEURL = `https://jsonplaceholder.typicode.com/`;
const BASEURL_OPEN_WEATHER_MAP =
	'http://api.openweathermap.org/data/2.5/weather';
const API_KEY_OPEN_WEATHER_MAP_DATA = '03d92e1dcdf06982a35783083f52690a';

export { BASEURL, BASEURL_OPEN_WEATHER_MAP, API_KEY_OPEN_WEATHER_MAP_DATA };
