import { BASEURL } from './api.js';

function nestedQueriesCallback() {
	function getAnswer(url) {
		const result = new Promise((resolve, reject) => {
			fetch(url)
				.then(answer => answer.json())
				.then(data => resolve(data))
				.catch(error => reject(error));
		});

		return result;
	}

	function removeObjectPropertyInArray(array, property) {
		array.forEach(object => delete object[property]);
	}

	function postsHandler(arrPosts) {
		arrPosts.forEach(post => {
			const urlComments = BASEURL + 'comments?postId=' + post.id;
			const commentsData = getAnswer(urlComments);

			commentsData.then(arrComments => {
				removeObjectPropertyInArray(arrComments, 'postId');
				post.comments = arrComments;
			});

			delete post.userId;
		});
	}

	function usersHandler(arrUsers) {
		arrUsers.forEach(user => {
			const urlPosts = BASEURL + 'posts?userId=' + user.id;
			const postData = getAnswer(urlPosts);

			const urlTodos = BASEURL + 'todos?userId=' + user.id;
			const todosData = getAnswer(urlTodos);

			postData.then(data => {
				user.posts = data;
				postsHandler(data);
			});

			todosData.then(arrTodos => {
				removeObjectPropertyInArray(arrTodos, 'userId');
				user.todos = arrTodos;
			});
		});

		console.log(arrUsers);
	}

	const urlUsers = BASEURL + 'users';
	const usersData = getAnswer(urlUsers);

	usersData.then(usersHandler);
}

export { nestedQueriesCallback };
