import { BASEURL } from './api.js';

async function nestedQueriesAsync() {
	const answerUsers = await fetch(BASEURL + 'users');
	const usersArr = await answerUsers.json();

	usersArr.forEach(async user => {
		const answerPosts = await fetch(BASEURL + 'posts?userId=' + user.id);
		const arrPosts = await answerPosts.json();

		const answerTodos = await fetch(BASEURL + 'todos?userId=' + user.id);
		const arrTodos = await answerTodos.json();

		arrPosts.forEach(async post => {
			const answerComments = await fetch(BASEURL + 'comments?postId=' + post.id);
			const commentsArr = await answerComments.json();

			delete post.userId;
			commentsArr.forEach(comment => {delete comment.postId})
			post.comments = commentsArr;
		});


		arrTodos.forEach(todo => {delete todo.userId})
		user.todos = arrTodos;
		user.posts = arrPosts;
	});


	console.log(usersArr);
}

export { nestedQueriesAsync };
