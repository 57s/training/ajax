import { BASEURL } from './api.js';

// Get
function XMLHttpRequestExamplePost() {
	const newPost = { title: 'My Post', body: 'Text my post', userId: 105 };

	function handler() {
		console.log(JSON.parse(this.responseText));
	}

	//0. Создание экземпляра запроса
	const xhr = new XMLHttpRequest();

	//1. Как и куда отправить
	xhr.open('POST', 'https://jsonplaceholder.typicode.com/posts');

	//1.1 Добавление заголовка
	xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');

	//2. Отправка запроса
	xhr.send(JSON.stringify(newPost));

	//3. Обработка запроса
	xhr.addEventListener('load', handler);
	// Альтернативный вариант присвоить обработчик
	// xhr.onload = handler;
}

export { XMLHttpRequestExamplePost };
