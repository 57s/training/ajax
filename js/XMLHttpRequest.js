import { BASEURL } from './api.js';

// GET
function getXhr() {
	const url = BASEURL + 'posts';
	const xhr = new XMLHttpRequest();

	const handler = function () {
		const data = JSON.parse(xhr.response);
		console.log(data);
	};

	xhr.open('GET', url);
	xhr.send();
	xhr.onload = handler;
}

// POST
function postXhr() {
	const url = BASEURL + 'posts';
	const xhr = new XMLHttpRequest();
	const newPost = { title: 'My Post', body: 'Text my post', userId: 105 };

	const handler = function () {
		const data = JSON.parse(xhr.response);
		console.log(data);
	};

	xhr.open('POST', url);
	xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
	xhr.send(JSON.stringify(newPost));
	xhr.onload = handler;
}

// PUT
function putXhr(num = 1) {
	const url = BASEURL + 'posts/' + num;
	const xhr = new XMLHttpRequest();
	const changedPost = {
		title: 'Changed Title',
		body: 'Changed Body',
		userId: 77,
	};

	const handler = function () {
		const data = JSON.parse(xhr.response);
		console.log(data);
	};

	xhr.open('PUT', url);
	xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
	xhr.send(JSON.stringify(changedPost));
	xhr.onload = handler;
}

// PATCH
function patchXhr(num = 1) {
	const url = BASEURL + 'posts/' + num;
	const xhr = new XMLHttpRequest();
	const modPost = { title: 'NEW Title' };

	const handler = function () {
		const data = JSON.parse(xhr.response);
		console.log(data);
	};

	xhr.open('PATCH', url);
	xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
	xhr.send(JSON.stringify(modPost));
	xhr.onload = handler;
}

// DELETE
function deleteXhr(num = 1) {
	const url = BASEURL + 'posts/' + num;
	const xhr = new XMLHttpRequest();

	const handler = function () {
		const data = JSON.parse(xhr.response);
		console.log(data);
	};

	xhr.open('DELETE', url);
	xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
	xhr.send();
	xhr.onload = handler;
}

export { getXhr, postXhr, putXhr, patchXhr, deleteXhr };
