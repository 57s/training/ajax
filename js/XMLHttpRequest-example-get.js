import { BASEURL } from './api.js';

// Get
function XMLHttpRequestExampleGet() {
	function reqListener() {
		console.log(JSON.parse(this.responseText));
	}

	//0. Создание экземпляра запроса
	const req = new XMLHttpRequest();

	//1. Как и куда отправить
	req.open('GET', 'https://jsonplaceholder.typicode.com/posts');
	// Третий параметр false сделает запрос(код) синхронным
	// req.open('GET', 'https://jsonplaceholder.typicode.com/posts', false);

	//2. отправка запроса
	req.send();

	//3. Обработка запроса
	req.addEventListener('load', reqListener);
	// Альтернативный вариант присвоить обработчик
	// req.onload = reqListener;
}

export { XMLHttpRequestExampleGet };
