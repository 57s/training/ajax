import { BASEURL } from './api.js';

// GET
function axiosGet() {
	const url = BASEURL + 'posts';
	axios.get(url).then(data => console.log(data));
}

// POST
const newPost = { title: 'My Post', body: 'Text my post', userId: 107 };

function axiosPost() {
	const url = BASEURL + 'posts';
	axios.post(url, newPost).then(data => console.log(data));
}

// PUT
const changePost = { title: 'New Title', body: 'New body', userId: 101 };

function axiosPut(num = 1) {
	const url = BASEURL + 'posts/' + num;

	axios.put(url, changePost).then(data => console.log(data));
}

// PATCH
const modPost = { title: 'Mod Post', body: 'Mod Text my post' };

function axiosPatch(num = 1) {
	const url = BASEURL + 'posts/' + num;

	axios.patch(url, modPost).then(data => console.log(data));
}

// Delete
function axiosDelete(num = 1) {
	const url = BASEURL + 'posts/' + num;

	axios.delete(url).then(data => console.log(data));
}

export { axiosGet, axiosPost, axiosPut, axiosPatch, axiosDelete };
