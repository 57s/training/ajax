import { BASEURL } from './api.js';

// Get posts
const get = function (num = 1) {
	fetch(BASEURL + 'posts/' + num)
		.then(answer => answer.json())
		.then(data => console.log(data));
};

// Create post

const newPost = JSON.stringify({
	title: 'My Post',
	body: 'Text my post',
	userId: 107,
});

const postHeaders = {
	'Content-type': 'application/json; charset=UTF-8',
};

const postConfig = {
	method: 'POST',
	body: newPost,
	headers: postHeaders,
};

const post = function () {
	const url = BASEURL + 'posts';

	fetch(url, postConfig)
		.then(response => response.json())
		.then(data => console.log(data));
};

// Update post

const putBody = JSON.stringify({
	id: 1,
	title: 'foo',
	body: 'bar',
	userId: 1,
});

const putHeaders = {
	'Content-type': 'application/json; charset=UTF-8',
};

const putConfig = {
	method: 'PUT',
	body: putBody,
	headers: putHeaders,
};

const put = function (num = 1) {
	const url = BASEURL + 'posts/' + num;

	fetch(url, putConfig)
		.then(response => response.json())
		.then(data => console.log(data));
};

// Modify
const patchBody = JSON.stringify({
	title: 'New Title',
});

const patchHeaders = {
	'Content-type': 'application/json; charset=UTF-8',
};

const patchConfig = {
	method: 'PATCH',
	body: patchBody,
	headers: patchHeaders,
};

const patch = function (num = 1) {
	const url = BASEURL + 'posts/' + num;

	fetch(url, patchConfig)
		.then(response => response.json())
		.then(json => console.log(json));
};

// Delete
const delet = function (num) {
	const url = BASEURL + 'posts/' + num;

	fetch(url, { method: 'DELETE' })
		.then(response => response.json())
		.then(json => console.log(json));
};

export { get, post, put, patch, delet };
