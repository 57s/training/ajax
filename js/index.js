// import { get, post, put, patch, delet } from './fetch.js';

// import {
// 	getTodos,
// 	addTodo,
// 	changeTodo,
// 	modTodo,
// 	removeTodo,

// } from './fetch-async.js';
// import { nestedQueries } from './nested-queries.js';
// import { nestedQueriesAsync } from './nested-queries-async.js';
// import { nestedQueriesAsyncCallback } from './nested-queries-async-callback.js';
// import { nestedQueriesCallback } from './nested-queries-callback.js';

// import {
// 	axiosGet,
// 	axiosPost,
// 	axiosPut,
// 	axiosPatch,
// 	axiosDelete,
// } from './axios.js';

// import { XMLHttpRequestExampleGet } from './XMLHttpRequest-example-get.js';
// import { XMLHttpRequestExamplePost } from './XMLHttpRequest-example-post.js';
// import {
// 	getXhr,
// 	postXhr,
// 	putXhr,
// 	patchXhr,
// 	deleteXhr,
// } from './XMLHttpRequest.js';

// import { xhrNestedCallback } from './XMLHttpRequest-nested-callback.js';
// import { xhrNested } from './XMLHttpRequest-nested.js';

// import { getRequest } from './getRequest.js';
// import { getRequestAsync } from './getRequest-async.js';

// getRequestAsync('https://jsonplaceholder.typicode.com/posts')
// 	.then(console.log)
// 	.catch(console.log);

import { getOpenWeatherMapData } from './open-weather-map-fetch.js';

getOpenWeatherMapData();
