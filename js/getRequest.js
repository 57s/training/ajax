function getRequest(url) {
	const handler = (resolve, reject) => {
		const data = {};

		fetch(url)
			.then(answer => {
				data.ok = answer.ok;
				data.status = answer.status;
				return answer.json();
			})
			.then(json => {
				data.data = json;
				resolve(data);
			})
			.catch(error => reject(error));
	};

	return new Promise(handler);
}

export { getRequest };
