import { BASEURL } from './api.js';

async function nestedQueriesAsyncCallback() {
	const getAnswer = async url => {
		const answer = await fetch(url);
		const data = await answer.json();

		return data;
	};

	const removeObjectPropertyInArray = (array, property) => {
		array.forEach(object => delete object[property]);
	};

	const postHandler = async function (post) {
		const urlComments = BASEURL + 'comments?postId=' + post.id;
		const answerComments = await fetch(urlComments);
		const commentsArr = await answerComments.json();

		removeObjectPropertyInArray(commentsArr, 'postId');
		post.comments = commentsArr;
		delete post.userId;
	};

	const userHandler = async function (user) {
		const urlPosts = BASEURL + 'posts?userId=' + user.id;
		const answerPosts = await fetch(urlPosts);
		const arrPosts = await answerPosts.json();

		const urlTodos = BASEURL + 'todos?userId=' + user.id;
		const answerTodos = await fetch(urlTodos);
		const arrTodos = await answerTodos.json();

		arrPosts.forEach(postHandler);
		removeObjectPropertyInArray(arrTodos, 'userId');
		user.posts = arrPosts;
		user.todos = arrTodos;
	};

	async function getUsers() {
		const urlUsers = BASEURL + 'users';
		const usersArr = await getAnswer(urlUsers);

		usersArr.forEach(userHandler);
		console.log(usersArr);
	}

	getUsers();
}

export { nestedQueriesAsyncCallback };
