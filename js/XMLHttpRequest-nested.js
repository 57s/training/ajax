// 😅
import { BASEURL } from './api.js';

function xhrNested() {
	const urlUser = BASEURL + 'users';

	const xhrUser = new XMLHttpRequest();

	xhrUser.open('GET', urlUser);
	xhrUser.send();
	xhrUser.onload = function () {
		const arrUsers = JSON.parse(this.response);

		arrUsers.forEach(user => {
			const urlTodos = BASEURL + 'todos?userId=' + user.id;
			const urlPosts = BASEURL + 'posts?userId=' + user.id;

			const xhrTodos = new XMLHttpRequest();
			const xhrPosts = new XMLHttpRequest();

			xhrTodos.open('GET', urlTodos);
			xhrTodos.send();
			xhrTodos.onload = function () {
				const arrTodos = JSON.parse(xhrTodos.response);

				arrTodos.forEach(todo => delete todo.userId)
				user.todos = arrTodos;
			};

			xhrPosts.open('GET', urlPosts);
			xhrPosts.send();
			xhrPosts.onload = function () {
				const arrPost = JSON.parse(xhrPosts.response);

				arrPost.forEach(post => {
					const urlComments = BASEURL + 'comments?postId=' + post.id;
					const xhrComments = new XMLHttpRequest();

					xhrComments.open('GET', urlComments);
					xhrComments.send();
					xhrComments.onload = function () {
						const arrComments = JSON.parse(xhrComments.response);
						arrComments.forEach(comment => delete comment.postId)
						post.comments = arrComments;
					}
					delete post.userId;
				})

				user.posts = arrPost;
			};
		});

		console.log(arrUsers);
	};
}

export { xhrNested };
