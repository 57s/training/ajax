function fetchPostExample() {
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',

		body: JSON.stringify({
			title: 'My Post',
			body: 'Text my post',
			userId: 1,
		}),

		headers: {
			'Content-type': 'application/json; charset=UTF-8',
		},
	})
		.then(response => response.json())
		.then(json => console.log(json));
}

export { fetchPostExample };
