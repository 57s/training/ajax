import { BASEURL } from './api.js';

function xhrNestedCallback() {
	function getData(url, callback) {
		const xhr = new XMLHttpRequest();

		const handler = function () {
			const data = JSON.parse(this.response);
			callback(data);
		};

		xhr.open('GET', url);
		xhr.send();
		xhr.onload = handler;
	}

	function handlerComments(arrComments, post) {
		arrComments.forEach(comment => {
			delete comment.postId;
		});

		post.comments = arrComments;
	}

	function handlerPosts(arrPosts, user) {
		arrPosts.forEach(post => {
			const urlComments = BASEURL + 'comments?postId=' + post.id;

			getData(urlComments, arrComments => handlerComments(arrComments, post));
			delete post.userId;
		});

		user.posts = arrPosts;
	}

	function handlerTodos(arrTodos, user) {
		arrTodos.forEach(todo => {
			delete todo.userId;
		});

		user.todos = arrTodos;
	}

	function handlerUsers(arrUsers) {
		arrUsers.forEach(user => {
			const urlPosts = BASEURL + 'posts?userId=' + user.id;
			const urlTodos = BASEURL + 'todos?userId=' + user.id;

			getData(urlPosts, arrPosts => handlerPosts(arrPosts, user));
			getData(urlTodos, arrTodos => handlerTodos(arrTodos, user));
		});

		console.log(arrUsers);
	}

	function getUsers() {
		const urlUser = BASEURL + 'users';

		getData(urlUser, handlerUsers);
	}

	getUsers();
}

export { xhrNestedCallback };
