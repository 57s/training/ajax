import { BASEURL } from './api.js';

const headers = new Headers();
headers.append('Content-type', 'application/json; charset=UTF-8');

// Get todos
async function getTodos() {
	const url = BASEURL + 'todos';
	const response = await fetch(url);
	const data = await response.json();

	console.log('Response = ', response);
	console.log('Data = ', data);
}

// POST Add todo
async function addTodo() {
	const url = BASEURL + 'todos';

	const newTodo = {
		userId: 5,
		title: 'New Todo',
		completed: true,
	};

	const config = {
		method: 'POST',
		body: JSON.stringify(newTodo),
		headers: headers,
	};

	const response = await fetch(url, config);
	const data = await response.json();

	console.log('Response = ', response);
	console.log('Data = ', data);
}

// PUT Change todo
async function changeTodo(num = 1) {
	const url = BASEURL + 'todos/' + num;

	const changeTodo = {
		userId: 5,
		title: 'Change Todo',
		completed: false,
	};

	const config = {
		method: 'PUT',
		body: JSON.stringify(changeTodo),
		headers: headers,
	};

	const response = await fetch(url, config);
	const data = await response.json();

	console.log('Response = ', response);
	console.log('Data = ', data);
}

// PATCH Modify todo
async function modTodo(num = 1) {
	const url = BASEURL + 'todos/' + num;

	const modTodo = {
		userId: 10,
		title: 'Mod Todo',
		completed: true,
	};

	const config = {
		method: 'PATCH',
		body: JSON.stringify(modTodo),
		headers: headers,
	};

	const response = await fetch(url, config);
	const data = await response.json();

	console.log('Response = ', response);
	console.log('Data = ', data);
}

// DELETE Remove todo
async function removeTodo(num = 1) {
	const url = BASEURL + 'todos/' + num;

	const config = { method: 'DELETE' };

	const response = await fetch(url, config);
	const data = await response.json();

	console.log('Response = ', response);
	console.log('Data = ', data);
}
export { getTodos, addTodo, changeTodo, modTodo, removeTodo };
