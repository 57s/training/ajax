import {
	BASEURL_OPEN_WEATHER_MAP,
	API_KEY_OPEN_WEATHER_MAP_DATA,
} from './api.js';

function getOpenWeatherMapData() {
	const params = {
		q: 'Мценск',
		units: 'metric',
		lang: 'ru',
		appid: API_KEY_OPEN_WEATHER_MAP_DATA,
	};

	const query = new URLSearchParams(params).toString();

	fetch(BASEURL_OPEN_WEATHER_MAP + '?' + query)
		.then(response => response.json())
		.then(handler)
		.catch(console.error);

	function handler(data) {
		console.log(`Город: ${data.name}`);
		console.log(`Температура: ${data.main.temp} °C`);
		console.log(`Описание: ${data.weather[0].description}`);
	}
}

export { getOpenWeatherMapData };
