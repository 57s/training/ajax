async function getRequestAsync(url) {
	const result = {};
	const answer = await fetch(url);
	const json = await answer.json();

	result.ok = answer.ok;
	result.status = answer.status;
	result.data = json;

	return result;
}

export { getRequestAsync };
