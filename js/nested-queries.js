import { BASEURL } from './api.js';

function nestedQueries() {
	// users
	fetch(BASEURL + 'users')
		.then(answerUsers => answerUsers.json())
		.then(usersArr => usersArr.map(user => {
				// todos
				fetch(BASEURL + 'todos?userId=' + user.id)
					.then(answerTodos => answerTodos.json())
					.then(arrTodos => arrTodos.map(todo => {
						delete todo.userId;
						return todo;
					}))
					.then(arrTodos => (user.todos = arrTodos));

				// post
				fetch(BASEURL + 'posts?userId=' + user.id)
					.then(answerPost => answerPost.json())
					.then(arrPost => arrPost.map(post => {
							// comments
							fetch(BASEURL + 'comments?postId=' + post.id)
								.then(answerComments => answerComments.json())
								.then(arrComments => arrComments.map(comment => {
									delete comment.postId;
									return comment;
								}))
								.then(arrComments => (post.comments = arrComments));

							delete post.userId;

							return post;
						})
					)

					.then(postArr => (user.posts = postArr));

				return user;
			})
		)

		.then(data => console.log(data));
}

export { nestedQueries };
